####################
import sys,glob,os
import argparse,textwrap
import pandas as pd
#-------------------
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sb
####################
################################################################################ arguments
M_parser = argparse.ArgumentParser(description=textwrap.dedent('''\n
	Targeted-seq CNV caller.
	\n'''),add_help=False,formatter_class=argparse.RawTextHelpFormatter)
S_version = '1.1.0'
#-------------------------------------------------------------------------------
M_parser_Required = M_parser.add_argument_group('Required arguments')
M_parser_Required.add_argument('-ip','--input-path',type=str,required=True,
	help=textwrap.dedent('''\
		* Required: Please provide the PATH of sample.DB text file or DIRECTORY PATH of HsMetrics text files.
		\n'''))
M_parser_Required.add_argument('-odir','--output-directory',type=str,required=True,
	help=textwrap.dedent('''\
		* Required: Please provide the PATH of output directory.
		\n'''))
#-------------------------------------------------------------------------------
M_parser_Optional = M_parser.add_argument_group('Optional arguments')
M_parser_Optional.add_argument('-it','--input-type',type=str,
	default='db',
	choices=['db','dir'],
	help=textwrap.dedent('''\
	Please provide the TYPE of input.
	default) db
	explain) db = A text file of sample.DB, dir = A directory of HsMetrics files
		\n'''))
M_parser_Optional.add_argument('-ib','--input-bed',type=str,
	default='/data/ref/CNV/BEDs/LiquidSCAN_IVD_v1.2_Regions.gene.bed',
	help=textwrap.dedent('''\
	Please provide the PATH of bed file.
	default) /data/ref/CNV/BEDs/LiquidSCAN_IVD_v1.2_Regions.gene.bed
		\n'''))
M_parser_Optional.add_argument('-pon','--panelofnormal',type=str,
	default='/data/ref/CNV/PONs/LiquidSCAN_IVD_v1.2_cfDNA_PON.tsv',
	help=textwrap.dedent('''\
	Please provide the PATH of PON file.
	default) /data/ref/CNV/PONs/LiquidSCAN_IVD_v1.2_cfDNA_PON.tsv
		\n'''))
M_parser_Optional.add_argument('-cyt','--cytoband',type=str,
	default='/data/ref/hg19/hg19.cytoband.txt',
	help=textwrap.dedent('''\
	Please provide the PATH of cytoband file.
	default) /data/ref/hg19/hg19.cytoband.txt
		\n'''))
M_parser_Optional.add_argument('-cf','--cut-off',type=float,
	default=5,
	help=textwrap.dedent('''\
	Please provide the VALUE of cut-off.
	default) 5
		\n'''))
M_parser_Optional.add_argument('-v','--version', action='version', version=S_version)
M_parser_Optional.add_argument('-h','--help',action='help', help='show this help message and exit')
#===============================================================================
M_args = M_parser.parse_args()
#-------------------------------------------------------------------------------
S_inPath = M_args.input_path
S_outDir = M_args.output_directory
S_inType = M_args.input_type
S_inBED = M_args.input_bed
S_PON = M_args.panelofnormal
S_Cyto = M_args.cytoband
F_inCF = M_args.cut_off
#===============================================================================
S_outPlotDir = '{}/CNV'.format(S_outDir)
os.system('mkdir {}'.format(S_outPlotDir))
#===============================================================================
L_TargetGENE = ['ERBB2','MET','MYCN']
df_PON = pd.read_csv(S_PON, sep='\t')
D_Cyto = dict(pd.read_csv(S_Cyto, sep='\t').values)
################################################################################ functions
def GENE2anno(df, GeneList) :
	L_DFs = []	
	for S_gene in GeneList :
		df_selected = df[(df['gene']==S_gene)]
		L_DFs.append(df_selected)
	df_Concated = pd.concat(L_DFs,ignore_index=True)
	return(df_Concated)
#===============================================================================
def SampleDB2anno(SampleDB, bed) :
	L_inFiles = sorted(SampleDB)
	df_inBED = pd.read_csv(bed,sep='\t',names=['chrom','start','end','gene'])
	df_inBED['start'] = df_inBED['start']+1
	#---------------------------------------------------------------------------
	df_GeneAnno = pd.merge(df_inBED,
					pd.read_csv(L_inFiles[0],sep='\t',usecols=[0,1,2,7]),
					how='left',
					on=['chrom','start','end']).rename(columns={'normalized_coverage':L_inFiles[0].split('/')[-1].split('.')[0]})
	#---------------------------------------------------------------------------
	for S_inIOR in L_inFiles[1:] :
		S_inName = S_inIOR.split('/')[-1].split('.')[0]
		df_inIOR = pd.read_csv(S_inIOR,sep='\t',usecols=[0,1,2,7])
		df_GeneAnno = pd.merge(df_GeneAnno,df_inIOR,how='left',on=['chrom','start','end']).rename(columns={'normalized_coverage':S_inName})
	#---------------------------------------------------------------------------
	df_GeneAnno = GENE2anno(df_GeneAnno,L_TargetGENE)
	return(df_GeneAnno)
#===============================================================================
def IORs2anno(IORdir, bed) :
	L_inFiles = sorted(glob.glob('{}/*IOR.txt'.format(IORdir)))
	df_inBED = pd.read_csv(bed,sep='\t',names=['chrom','start','end','gene'])
	df_inBED['start'] = df_inBED['start']+1
	#---------------------------------------------------------------------------
	df_GeneAnno = pd.merge(df_inBED,
					pd.read_csv(L_inFiles[0],sep='\t',usecols=[0,1,2,7]),
					how='left',
					on=['chrom','start','end']).rename(columns={'normalized_coverage':L_inFiles[0].split('/')[-1].split('.')[0]})
	#---------------------------------------------------------------------------
	for S_inIOR in L_inFiles[1:] :
		S_inName = S_inIOR.split('/')[-1].split('.')[0]
		df_inIOR = pd.read_csv(S_inIOR,sep='\t',usecols=[0,1,2,7])
		df_GeneAnno = pd.merge(df_GeneAnno,df_inIOR,how='left',on=['chrom','start','end']).rename(columns={'normalized_coverage':S_inName})
	#---------------------------------------------------------------------------
	df_GeneAnno = GENE2anno(df_GeneAnno,L_TargetGENE)
	return(df_GeneAnno)
#===============================================================================
def GeneScore(df, GeneList, ColNum) :
	L_Rows = []
	for S_gene in GeneList :
		df_Selected = df[(df['gene']==S_gene)]
		I_RowNum = df_Selected.shape[0]
		for S_inName in df.columns[ColNum:] :
			A_Selected_Sum = df_Selected[S_inName].sum()
			F_GeneScore = A_Selected_Sum/I_RowNum
			L_Rows.append([S_inName,S_gene,F_GeneScore])
	df_GeneScored = pd.DataFrame(L_Rows,columns=['sample','gene','GeneScore'])
	return(df_GeneScored)
#===============================================================================
def Zscore(df, GeneList, PON) :
	L_Array = []
	for S_gene in GeneList :
		A_Targeted_GeneScore = df[(df['gene']==S_gene)]['GeneScore']
		F_PON_Target_Avg = PON[S_gene][0]
		F_PON_Target_Std = PON[S_gene][-1]
		A_Zscore = (A_Targeted_GeneScore-F_PON_Target_Avg)/F_PON_Target_Std
		L_Array.append(A_Zscore)
	A_Zscores = pd.concat(L_Array,ignore_index=True)
	df.insert(loc=df.shape[1],column='Zscore',value=A_Zscores)
	return(df)
#===============================================================================
def CopyNum(df, GeneList, PON) :
	L_Array = []
	for S_gene in GeneList :
		A_Targeted_Zscore = df[(df['gene']==S_gene)]['Zscore']
		F_PON_Target_Avg = PON[S_gene][0]
		F_PON_Target_Std = PON[S_gene][-1]
		F_PON_Target_CV = F_PON_Target_Std/F_PON_Target_Avg
		A_CopyNum = A_Targeted_Zscore*(F_PON_Target_Avg*F_PON_Target_CV*2)+2
		L_Array.append(A_CopyNum)
	A_CopyNum = pd.concat(L_Array,ignore_index=True)
	df.insert(loc=df.shape[1],column='CopyNum',value=A_CopyNum)
	return(df)
#===============================================================================
def ROIlevelPLOT(control, case) :
	df_Control = control
	df_Case = case
	#---------------------------------------------------------------------------
	A_Genes = df_Control['gene']+':'+[ '{0:0=2d}'.format(i) for i in df_Control.index ]
	I_NumRow = A_Genes.shape[0]
	#---------------------------------------------------------------------------
	L_Control_DFs = []
	for S_inName in df_Control.columns[6:] :
		A_ROIscore = df_Control[S_inName]
		df_Framed = pd.DataFrame({'gene':A_Genes,'sample':[S_inName]*I_NumRow,'normalized_coverage':A_ROIscore})
		L_Control_DFs.append(df_Framed)
	#---------------------------------------------------------------------------
	L_Case_DFs = []
	for S_inName in df_Case.columns[4:] :
		A_ROIscore = df_Case[S_inName]
		df_Framed = pd.DataFrame({'gene':A_Genes,'sample':[S_inName]*I_NumRow,'normalized_coverage':A_ROIscore})
		L_Case_DFs.append(df_Framed)
	#---------------------------------------------------------------------------
	df_Control4plot = pd.concat(L_Control_DFs,ignore_index=True)
	df_Case4plot = pd.concat(L_Case_DFs,ignore_index=True)
	I_Control_SampleCount = len(set(df_Control4plot['sample']))
	#---------------------------------------------------------------------------
	plt.figure(figsize=(30,10))
	plt.title('ROI level analysis',fontsize=25)
	sb.lineplot( data=df_Control4plot, x='gene', y='normalized_coverage', hue='sample', marker='o', palette=['black']*I_Control_SampleCount, alpha=0.5, legend=False )
	sb.lineplot( data=df_Case4plot, x='gene', y='normalized_coverage', hue='sample', lw=5, marker='o' )
	#plt.ylim(0,5)
	plt.xlabel('gene',fontsize=20)
	plt.ylabel('normalized_coverage',fontsize=20)
	plt.xticks(rotation=90,fontsize=13)
	plt.grid(linestyle='--')
	plt.legend(fontsize=17,bbox_to_anchor=(1,1),loc="upper left")
	plt.savefig('{}/ROIlevel.png'.format(S_outPlotDir),bbox_inches='tight')
#===============================================================================
def GenelevelPLOT(control, case) :
	plt.figure(figsize=(10,8))
	plt.title('GENE level analysis',fontsize=20)
	sb.boxplot(data=control, x="gene", y="Zscore", showfliers=False, palette=['whitesmoke']*len(L_TargetGENE))
	sb.swarmplot( data=case, x="gene", y="Zscore",hue='sample', size=10 )
	plt.xlabel('gene',fontsize=15)
	plt.ylabel('z-score',fontsize=15)
	plt.axhline(y=F_inCF, color='r', linestyle='-')
	plt.axhline(y=-F_inCF, color='r', linestyle='-')
	plt.grid(axis='y',linestyle='--')
	plt.legend(fontsize=13,bbox_to_anchor=(1,1),loc="upper left")
	plt.savefig('{}/Genelevel.png'.format(S_outPlotDir),bbox_inches='tight')
#===============================================================================
def CopyNumPLOT(control, case) :
	plt.figure(figsize=(10,8))
	plt.title('Copy Number analysis',fontsize=20)
	sb.boxplot(data=control, x="gene", y="CopyNum", showfliers=False, palette=['whitesmoke']*len(L_TargetGENE))
	sb.swarmplot( data=case, x="gene", y="CopyNum",hue='sample', size=10 )
	plt.xlabel('gene',fontsize=15)
	plt.ylabel('Copy Number',fontsize=15)
	plt.grid(axis='y',linestyle='--')
	plt.legend(fontsize=13,bbox_to_anchor=(1,1),loc="upper left")
	plt.savefig('{}/CopyNum.png'.format(S_outPlotDir),bbox_inches='tight')
#===============================================================================
def ResultTSV(control, case, threshold, cytoband, Type, path) :
	L_Cyto = []
	L_Confirm = []
	L_Alteration = []
	for I_index in case.index :
		S_gene = case['gene'][I_index]
		S_Cyto = cytoband[S_gene]
		F_Zscore = case['Zscore'][I_index]
		F_ControlMax = control[(control['gene']==S_gene)]['Zscore'].max()
		F_ControlMin = control[(control['gene']==S_gene)]['Zscore'].min()
		L_Cyto.append(S_Cyto)
		if F_ControlMax < F_Zscore <= threshold :
			L_Confirm.append('suspected')
			L_Alteration.append('Amplification')
		elif threshold < F_Zscore :
			L_Confirm.append('Y')
			L_Alteration.append('Amplification')
		elif -threshold < F_Zscore <= F_ControlMin :
			L_Confirm.append('suspected')
			L_Alteration.append('Deletion')
		elif F_Zscore <= -threshold :
			L_Confirm.append('Y')
			L_Alteration.append('Deletion')
		else :
			L_Confirm.append('-')
			L_Alteration.append('-')
	case.insert(loc=2,column='Band',value=L_Cyto)
	case.insert(loc=case.shape[1],column='CALL',value=L_Confirm)
	case.insert(loc=case.shape[1],column='ALTERATION',value=L_Alteration)
	case = case.rename(columns={'sample':'SID','gene':'Gene'})
	if Type == 'db' :
		df_SampleDB = pd.read_csv(path,sep='\t')[['PID','SID','TYPE']]
		case = pd.merge(df_SampleDB,case,how='inner',on=['SID'])
	case.to_csv('{}/CNV_Result.tsv'.format(S_outPlotDir),sep='\t',index=None)
################################################################################ generate DATAFRAMEs
if S_inType == 'db' :
	df_SampleDB = pd.read_csv(S_inPath,sep='\t')
	L_inIORs = []
	for I_index in df_SampleDB.index :
		S_SID = df_SampleDB['SID'][I_index]
		S_Path = df_SampleDB['PATH'][I_index]
		if S_SID.split('_')[-2] != 'XN' :
			S_HsMetricsPath = '{}/{}.ddcsp_smerged.bam.hsmatrics_IOR.txt'.format(S_Path,S_SID)
			L_inIORs.append(S_HsMetricsPath)
	df_Case_Targeted = SampleDB2anno(L_inIORs,S_inBED)
elif S_inType == 'dir' :
	df_Case_Targeted = IORs2anno(S_inPath,S_inBED)
#===============================================================================
df_PON_Targeted = GENE2anno(df_PON,L_TargetGENE)
#===============================================================================
df_Case_Targeted_GeneScore = GeneScore(df_Case_Targeted, L_TargetGENE, 4)
df_PON_Targeted_GeneScore = GeneScore(df_PON_Targeted, L_TargetGENE, 6)
#===============================================================================
D_PON_Info = {}
for S_gene in L_TargetGENE :
	A_PON_Target_GenScore = df_PON_Targeted_GeneScore[(df_PON_Targeted_GeneScore['gene']==S_gene)]['GeneScore']
	F_PON_Target_Avg = A_PON_Target_GenScore.mean()
	F_PON_Target_Std = A_PON_Target_GenScore.std()
	D_PON_Info[S_gene] = [F_PON_Target_Avg,F_PON_Target_Std]
#===============================================================================
df_PON_Targeted_GeneScoreZscore = Zscore(df_PON_Targeted_GeneScore, L_TargetGENE, D_PON_Info)
df_Case_Targeted_GeneScoreZscore = Zscore(df_Case_Targeted_GeneScore, L_TargetGENE, D_PON_Info)
#===============================================================================
df_PON_Targeted_GeneScoreZscoreCopyNum = CopyNum(df_PON_Targeted_GeneScoreZscore, L_TargetGENE, D_PON_Info)
df_Case_Targeted_GeneScoreZscoreCopyNum = CopyNum(df_Case_Targeted_GeneScoreZscore, L_TargetGENE, D_PON_Info)
#===============================================================================
################################################################################ generate PLOTs
#ROIlevelPLOT(df_PON_Targeted,df_Case_Targeted)
#GenelevelPLOT(df_PON_Targeted_GeneScoreZscore,df_Case_Targeted_GeneScoreZscore)
#CopyNumPLOT(df_PON_Targeted_GeneScoreZscoreCopyNum,df_Case_Targeted_GeneScoreZscoreCopyNum)
################################################################################ generate RESULTs
ResultTSV(df_PON_Targeted_GeneScoreZscoreCopyNum, df_Case_Targeted_GeneScoreZscoreCopyNum, F_inCF, D_Cyto, S_inType, S_inPath)
################################################################################
