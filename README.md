# Targeted-seq CNV caller
### Detect Copy Number Variantion(CNV) in a plasma sample using targeted sequence data.

## GETTING HELP
If your problem or question is still unsolved, feel free to post a question or submit a bug report on the e-mail(dk.nam@kr-geninus.com).

## QUICKSTART GUIDE
Obtain the required modules:
`pip install numpy pandas matplotlib seaborn`  
You may need to install pip first, and depending on preferences you may add sudo at the beginning of this command.

## REQUIREMENTS
This caller was developed and tested using Python3.7. Using any other version may cause errors or faulty results.

HsMetrics(PICARD JDK API)

The list of python packages required and tested versions as reported by `pip freeze`:  
pandas==0.24.2  
matplotlib==2.2.2  
seaborn==0.7.1

### script run
To get CNV plots and result files, use LS_CNV_Caller.py:  
`python LS_CNV_Caller.py -ip [input path] -odir [output directory] -ib [input bed file] -pon [panel of normal]`

The target ROI.bed file specified here is GENINUS target ROI. You can take any .bed file here (assuming it fits in memory).    
There are some parameters to adjust trimming, quality filter and variants call.

### In general
To improve your results you probably want to change a few parameters. Most of the values used in caller can be altered using arguments. To find out what arguments can be passed into any script, try running it with -h as argument, for example:  
`python LS_CNV_Caller.py -h`  

### Description
	usage: LS_CNV_Caller.py -ip INPUT_PATH -odir OUTPUT_DIRECTORY
								   [-it {txt,dir}] [-ib INPUT_BED]
								   [-pon PANELOFNORMAL] [-cf CUT_OFF] [-v] [-h]

	Targeted-seq CNV caller.

	Required arguments:
	  -ip INPUT_PATH, --input-path INPUT_PATH
							* Required: Please provide the PATH of HsMetrics text file or DIRECTORY PATH of HsMetrics text files.

	  -odir OUTPUT_DIRECTORY, --output-directory OUTPUT_DIRECTORY
							* Required: Please provide the PATH of output directory.


	Optional arguments:
	  -it {txt,dir}, --input-type {txt,dir}
							Please provide the TYPE of input.
							default) txt
							explain) txt = A text file of HsMetrics file, dir = A directory of HsMetrics files

	  -ib INPUT_BED, --input-bed INPUT_BED
							Please provide the PATH of bed file.
							default) /data5/users/dknam/src/CNV_Caller/BEDs/LiquidSCAN_IVD_v1.2_Regions.gene.bed

	  -pon PANELOFNORMAL, --panelofnormal PANELOFNORMAL
							Please provide the PATH of PON file.
							default) /data5/users/dknam/src/CNV_Caller/PONs/38samples_IVD_v1.2_cfDNA_PON.tsv

	  -cf CUT_OFF, --cut-off CUT_OFF
							Please provide the VALUE of cut-off.
							default) 5

	  -v, --version         show program's version number and exit
	  -h, --help            show this help message and exit